package main

import (
	"database/sql"
	"fmt"

	_ "github.com/lib/pq"
)

func main() {
	// Koneksi ke database PostgreSQL
	connStr := "host=172.22.0.4 user=postgres password=password dbname=testdb sslmode=disable"
	db, err := sql.Open("postgres", connStr)
	if err != nil {
		panic(err)
	}
	defer db.Close()

	// Membuat tabel jika belum ada
	_, err = db.Exec(`CREATE TABLE IF NOT EXISTS users (
        id SERIAL PRIMARY KEY,
        name TEXT
    )`)
	if err != nil {
		panic(err)
	}

	// Menambahkan data ke dalam tabel
	_, err = db.Exec(`INSERT INTO users (name) VALUES ($1)`, "John Doe")
	if err != nil {
		panic(err)
	}

	// Mengambil data dari tabel
	rows, err := db.Query("SELECT id, name FROM users")
	if err != nil {
		panic(err)
	}
	defer rows.Close()

	// Menampilkan data
	for rows.Next() {
		var id int
		var name string
		if err := rows.Scan(&id, &name); err != nil {
			panic(err)
		}
		fmt.Printf("ID: %d, Name: %s\n", id, name)
	}
}
